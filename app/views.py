from flask_restful import  Resource
from .auth.views import requires_auth


class Hello(Resource):
    @staticmethod
    def get(text):
        return {"message": "Hi! {}".format(text)}

    @requires_auth
    @staticmethod
    def post(text, other=None):
        return {"message": "Hi! {}".format(text)}
