from flask import render_template
from flask_api import FlaskAPI, status
from flask_bcrypt import Bcrypt
from flask_restful import Api

from instance.config import app_config


def create_app(config_name):
    from app.Entities import User

    from .auth.views import Registration, Login, CheckToken
    from .views import Hello

    app = FlaskAPI(__name__, instance_relative_config=True)

    app.debug = config_name == 'development'
    app.config.from_object(app_config[config_name])
    app.config['STATIC_FOLDER'] = 'template'

    api = Api(app, serve_challenge_on_401=True)

    api.add_resource(Registration, '/auth/register', methods=['POST'])
    api.add_resource(Login, '/auth/login', methods=['POST'])
    api.add_resource(CheckToken, '/auth/token/check', methods=['GET'])

    api.add_resource(Hello, '/hello/<text>', methods=['GET', 'POST'])

    @app.route('/')
    def index_page():
        return render_template('index.html')

    return app
