from flask import request, jsonify
from flask_bcrypt import Bcrypt
from flask_restful import Resource
from app.Entities.User import User

from functools import wraps


def requires_auth(f):
    @wraps(f)
    def auth(*args, **kwargs):
        try:
            auth_header = request.headers.get('Authorization')
            if not auth_header:
                response = jsonify({
                    'message': 'Key `Authorization` not found'
                })
                response.status_code = 401
                return response
            token_list = auth_header.split(" ")
            if len(token_list) != 2 or token_list[0] != 'Bearer':
                response = jsonify({
                    'message': 'Authorization type failed'
                })
                response.status_code = 401
                return response

            access_token = token_list[1]
            if not access_token:
                response = jsonify({
                    'message': 'Authorization token failed'
                })
                response.status_code = 401
                return response

            user_id = User.decode_token(access_token)
            if isinstance(user_id, str):
                # user is not legit, so the payload is an error message
                response = jsonify({
                    'message': user_id
                })
                response.status_code = 401
                return response
            kwargs['other'] = {'user_id': user_id}

            return f(*args, **kwargs)
        except Exception as e:
            response = jsonify({
                'message': str(e)
            })
            response.status_code = 500
            return response

    return auth


def requires_auth_admin(f):
    @wraps(f)
    def auth(*args, **kwargs):
        try:
            auth_header = request.headers.get('Authorization')
            if not auth_header:
                response = jsonify({
                    'message': 'Key `Authorization` not found'
                })
                response.status_code = 401
                return response
            token_list = auth_header.split(" ")
            if len(token_list) != 2 or token_list[0] != 'Bearer':
                response = jsonify({
                    'message': 'Authorization type failed'
                })
                response.status_code = 401
                return response

            access_token = token_list[1]
            if not access_token:
                response = jsonify({
                    'message': 'Authorization token failed'
                })
                response.status_code = 401
                return response

            user_id = User.decode_token(access_token)
            if isinstance(user_id, str):
                # user is not legit, so the payload is an error message
                response = jsonify({
                    'message': user_id
                })
                response.status_code = 401
                return response
            # test admin
            if user_id != 1:
                response = jsonify({
                    'message': "user not admin"
                })
                response.status_code = 403
                return response
            return f(*args, **kwargs)
        except Exception as e:
            response = jsonify({
                'message': str(e)
            })
            response.status_code = 500
            return response

    return auth


class Registration(Resource):
    @staticmethod
    def post():
        data = request.json
        if not data:
            response = jsonify({
                'message': 'Data not found.'
            })
            response.status_code = 400
            return response
        if 'email' not in data:
            response = jsonify({
                'message': 'Field `email` not found.'
            })
            response.status_code = 400
            return response
        if 'password' not in data:
            response = jsonify({
                'message': 'Field `password` not found.'
            })
            response.status_code = 400
            return response

        # Query to see if the user already exists
        user = User.select().where(User.email == data['email']).first()
        if not user:
            try:
                User.create(email=data['email'], password=Bcrypt().generate_password_hash(data['password']).decode())
                response = jsonify({
                    'message': 'You registered successfully. Please login.',
                })
                response.status_code = 201
                return response
            except Exception as e:
                response = jsonify({
                    'message': str(e)
                })
                response.status_code = 201
                return response

        response = jsonify({
            'message': 'User already exists. Please login.'
        })
        response.status_code = 202
        return response


class Login(Resource):
    @staticmethod
    def post():
        data = request.json
        if not data:
            response = jsonify({
                'message': 'Data not found.'
            })
            response.status_code = 400
            return response
        if 'email' not in data:
            response = jsonify({
                'message': 'Field `email` not found.'
            })
            response.status_code = 400
            return response
        if 'password' not in data:
            response = jsonify({
                'message': 'Field `password` not found.'
            })
            response.status_code = 400
            return response

        try:
            user = User.select().where(User.email == data['email']).first()
            if user and user.password_is_valid(data['password']):
                # Generate the access token
                access_token = user.generate_token(user.id)
                if access_token:
                    response = jsonify({
                        'message': 'You logged in successfully.',
                        'access_token': access_token.decode()
                    })
                    return response
            # User does not exist
            response = jsonify({
                'message': 'Invalid email or password, Please try again.'
            })
            response.status_code = 401
            return response
        except Exception as e:
            response = jsonify({
                'message': str(e)
            })
            response.status_code = 500
            return response


class CheckToken(Resource):
    @staticmethod
    @requires_auth
    def get(**kwargs):
        response = jsonify({
            'message': 'Good token'
        })
        return response
