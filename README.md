# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

**Summary of set up**
---
    pip install virtualenv

    git clone https://finvara@bitbucket.org/finvara/flask_api_skeleton.git

    cd flask_api_skeleton

**Configuration**
---
    virtualenv venv

*or*

    virtualenv -p python3 venv

**Dependencies**
---

- ubuntu: . venv/bin/activate
- windows: "venv/Scrips/activate"

*or*

    cd venv/Scripts
    activate
		
Next step

	pip install -r requirements.txt


**Database configuration**
---
go to settings.py and check database config

*Deployment instructions*
    
    python run.py
*or*

    python3 run.py
	
**Route**
	
    http://localhost:5003/hello/world in debug
*or*
    
    http://127.0.0.1:5000/hello/world in production