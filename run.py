import os
from app import create_app
from settings import DEBUG, set_environments
set_environments()

config_name = os.getenv('APP_SETTINGS')
app = create_app(config_name)


if __name__ == '__main__':
    if DEBUG:
        app.debug = True
        app.run(port=5003)
    else:
        app.run(host="127.0.0.1", port=5000)
