import base64
import logging
from os import environ as env, urandom
import platform
import time

from peewee import MySQLDatabase, OperationalError, Model
from playhouse.shortcuts import RetryOperationalError

def generate_base64_key():
    foo = urandom(24)
    foo_base64 = base64.b64encode(foo)
    return foo_base64


logger = logging.getLogger('Settings')
PLATFORM = platform.node()
print(logger)

if PLATFORM in ["my_platform"]:
    logger.info("Debug at %s" % PLATFORM)
    DEBUG = True

else:
    print("Deploy at %s" % PLATFORM)
    logger.info("Deploy at %s" % PLATFORM)
    DEBUG = False

DB_NAME = env.get('DB_NAME', "flask")
DB_USERNAME = env.get('DB_USERNAME', "root")
DB_HOST = env.get('DB_HOST', "127.0.0.1")
DB_PASSWORD = env.get('DB_PASSWORD', "")
DB_PORT = int(env.get('DB_PORT', 3306))
LOCAL_IP = env.get("LOCAL_IP", "127.0.0.1")

def set_environments():
    env['PLATFORM'] = PLATFORM
    env['APP_SETTINGS'] = 'development'
    env['FLASK_APP'] = 'run.py'
    env['SECRET'] = "my secret key for auth"

class MySQLRetryDatabase(RetryOperationalError, MySQLDatabase):
    """
    To make my DB retry after each failure
    """
    pass

db = MySQLRetryDatabase(DB_NAME, host=DB_HOST, user=DB_USERNAME, password=DB_PASSWORD, port=DB_PORT, charset='utf8mb4')
check_mysql = False
while not check_mysql:
    try:
        db.execute_sql("Select 2+2-1;")
        logger.info("Database is ready")
        check_mysql = True
    except OperationalError as e:
        message = "Database is unavailable or you'r db credentials is bad error{%s}" % e
        logger.info(message)
        print(message)
        time.sleep(10)

class BaseModel(Model):
    class Meta:
        database = db
